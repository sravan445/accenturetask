//
//  SRNetworkManager.swift
//  AccentureTask
//
//  Created by Sravan Reddy on 24/06/20.
//  Copyright © 2020 EHiOSDev2. All rights reserved.
//

import UIKit
import Foundation


class SRNetworkManager: NSObject {
    var responseType: ResponseType!
    
    @objc public enum ResponseType: Int {
        case json
    }
    
    typealias SuccessClosure = (Any?) -> Void
    typealias FailureClosure = (Error) -> Void
    typealias DownloadDataClosure = (Data?) -> Void
    
    var urlRequest : URLRequest!
    var dataTask : URLSessionDataTask?
    
    public override init() { }
    @objc func getResponse(type responseType: ResponseType, success: @escaping SuccessClosure, failure: @escaping FailureClosure)  {
    
        
        
        dataTask = URLSession.shared.dataTask(with: urlRequest) { (data, response , error) in
            // Handle in main thread to make UI changes
            OperationQueue.main.addOperation({
                guard let data = data, error == nil else {
                    switch URLError.Code(rawValue: (error?.code)!)
                    {
                    case .notConnectedToInternet:
                        print("NotConnectedToInternet")
                    case .timedOut:
                        print("\(String(describing: self.urlRequest.url)) Timeout")

                    default:
                        failure(error!)
                        break
                    }
                    return
                }
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any]
                    if let response = json {
                        switch responseType {

                        case .json: do {
                            success(response as Any)
                            print(response)
                            }
                        default:
                            break

                        }
                    }
                } catch {
                    switch URLError.Code(rawValue: error.code) {
                    case .notConnectedToInternet:
                        print("NotConnectedToInternet")

                    case .timedOut:
                        print("Timeout")

                    default:
                        let statusCode = (response as! HTTPURLResponse).statusCode
                        // Unauthorized status if token or user id is invalid
                        print(statusCode)
                        break
                    }
                }

            })
        }
        dataTask?.resume()
    }

}

extension Error {
    var code: Int { return (self as NSError).code }
    var domain: String { return (self as NSError).domain }
}
