//
//  ListViewController.swift
//  AccentureTask
//
//  Created by Sravan Reddy on 24/06/20.
//  Copyright © 2020 EHiOSDev2. All rights reserved.
//

import UIKit

class ListViewController: UIViewController {
    
    fileprivate let CellReuseIdentifier = "ListTableViewCellIdentifier"
    fileprivate let tableview = UITableView()

    
    var data: DataModel?
    var list = [DataDescription]()
    lazy var dataprovider: SRDataProvider = {
        return SRDataProvider()
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        configureTableView()
        loadData()
        
        // Do any additional setup after loading the view.
    }
    
    func configureTableView() {
        tableview.dataSource = self
        tableview.estimatedRowHeight = 100
        tableview.rowHeight = UITableView.automaticDimension
        tableview.register(TableViewCell.self, forCellReuseIdentifier: CellReuseIdentifier)
        
        view.addSubview(tableview)
        tableview.translatesAutoresizingMaskIntoConstraints = false
        tableview.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableview.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableview.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tableview.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    }

    
    func loadData(){
        
        dataprovider.loadAPI { (result) in
        guard let data = result else {return}
            OperationQueue.main.addOperation {
                self.data = data
                self.tableview.reloadData()
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ListViewController : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: CellReuseIdentifier, for: indexPath) as! TableViewCell
        let object = list[indexPath.row]
        cell.nameLabel.text = object.title
        cell.detailLabel.text = object.description
        return cell
    }
}

