//
//  DataModel.swift
//  AccentureTask
//
//  Created by Sravan Reddy on 24/06/20.
//  Copyright © 2020 EHiOSDev2. All rights reserved.
//

import UIKit

class DataModel: NSObject {
    
    var title : String?
    var rowsArray = [DataDescription]()
    
    init(with dict: [String: Any]) {
        title = dict[keys.title] as? String
        if let array = dict[keys.rows] as? [[String:Any]]
        {
            rowsArray.append(contentsOf: array.map{DataDescription(with: $0)})
        }
    }
}



struct keys {
    static let title = "title"
    static let rows = "rows"
    static let description = "description"
    static let imageHref = "imageHref"
}

 


