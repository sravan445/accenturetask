//
//  SRNetworkRequest.swift
//  AccentureTask
//
//  Created by Sravan Reddy on 24/06/20.
//  Copyright © 2020 EHiOSDev2. All rights reserved.
//

import UIKit

class SRNetworkRequest: NSObject {
    
    enum RequestType: String {
        case get    = "GET"
        case post = "POST"
    }
    
    enum ContentType: String {
        case contentJSON = "application/json"
    }
    
    class func getRequest(url: URL) -> URLRequest  {
        /// URLRequest
        var request = URLRequest(url: url)
        
        request.httpMethod = "GET"
        
        /// Content-Type
        request.addValue(ContentType.contentJSON.rawValue, forHTTPHeaderField: "Content-Type")
        return request
    }

}

extension SRNetworkRequest{
    
    struct path {
        static var contentAPI: URL {
            return URL(string:"https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json")!
        }

    }
    class APIClient
    {
        class func getAPI() -> SRNetworkManager
        {
            let request = getRequest(url:path.contentAPI)
            print(request)
            // reference the request to the manager to get response handler
            let manager = SRNetworkManager()
            manager.urlRequest = request
            return manager
        }
    }

}
