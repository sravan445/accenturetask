//
//  SRDataProvider.swift
//  AccentureTask
//
//  Created by Sravan Reddy on 24/06/20.
//  Copyright © 2020 EHiOSDev2. All rights reserved.
//

import UIKit

class SRDataProvider: NSObject {
    var manager: SRNetworkManager?
    override init() {
        
    }
    
    func loadAPI(completion: @escaping(DataModel?)-> Void){
        manager = SRNetworkRequest.APIClient.getAPI()
        manager?.getResponse(type: .json, success: { (response) in
            guard let dict = response as? [String: Any] else {
                completion(nil)
                return
            }
            let datamodel = DataModel(with: dict)
            completion(datamodel)

        }, failure: { (error) in
            print("Error while fetching : \(error.localizedDescription)")
            completion(nil)
        })
    }

}
