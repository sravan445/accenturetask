//
//  DataDescription.swift
//  AccentureTask
//
//  Created by Sravan Reddy on 24/06/20.
//  Copyright © 2020 EHiOSDev2. All rights reserved.
//

import UIKit

class DataDescription: NSObject {
    var title : String?
    var descriptionStr : String?
    var imageHref : String?

    init(with dict: [String: Any]) {
        title = dict[keys.title] as? String
        descriptionStr = dict[keys.description] as? String
        imageHref = dict[keys.imageHref] as? String
    }


}
